package com.epam.training.multithread;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;


public class App {
  public static void main(String[] args) {
    // this is the hash from the task but it will take ages to brute force it
    // String passwordHash = "4fd0101ea3d0f5abbe296ef97f47afec";

    // so I took something simple
    String passwordHash = "da8dbdf70c1180116caa00015de7d9f8";
    Integer queueCapacity = 100000;
    String characterSet = "abcdefghijklmnopqrstuvwxyz";

    try {
      Broker broker = new Broker(queueCapacity);

      int cores = Runtime.getRuntime().availableProcessors();
      ExecutorService threadPool = Executors.newFixedThreadPool(cores);

      for (int i = 0; i < cores - 1; i++) {
        threadPool.execute(new Consumer(Integer.toString(i), passwordHash, broker));
      }

      // for some reason works faster with one producer
      threadPool.execute(new Producer(broker, characterSet, 1, 1));
      // threadPool.execute(new Producer(broker, characterSet, 2, 2));
      // threadPool.execute(new Producer(broker, characterSet, 3, 4));
      // threadPool.execute(new Producer(broker, characterSet, 4, 4));

      threadPool.shutdown();
    }
    catch (Exception e) {
      System.out.println("Something went wrong");
    }
  }
}
