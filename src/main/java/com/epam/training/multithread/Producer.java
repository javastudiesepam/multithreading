package com.epam.training.multithread;

import java.util.Arrays;

public class Producer implements Runnable {
  private Integer step;
  private char[] characterSet;
  private char[] currentGuess;
  private Broker broker;

  public Producer(Broker broker, String characterSet, Integer startLength, Integer step) {
    this.step = step;
    this.broker = broker;
    this.characterSet = characterSet.toCharArray();

    currentGuess = new char[startLength];
    Arrays.fill(currentGuess, this.characterSet[0]);
  }

  @Override
  public void run() {
    while (broker.getContinueProducing()) {
      if (!broker.isFull()) {
        try {
          broker.put(getAttempt());
        } catch (InterruptedException e) {
          System.out.println("Something went wrong in a producer");
        }
        increment();
      }
    }
  }

  private void increment() {
    int index = currentGuess.length - 1;
    while (index >= 0) {
      // if reached z
      if (currentGuess[index] == characterSet[characterSet.length - 1]) {
        if (index == 0) {
          // increment guess length and fill with a
          currentGuess = new char[currentGuess.length + step];
          Arrays.fill(currentGuess, characterSet[0]);
          break;
        } else {
          currentGuess[index] = characterSet[0];
          index--;
        }
      } else {
        // increment the last letter
        currentGuess[index] = characterSet[Arrays.binarySearch(characterSet, currentGuess[index]) + 1];
        break;
      }
    }
  }

  private String getAttempt() {
    return String.valueOf(currentGuess);
  }
}
