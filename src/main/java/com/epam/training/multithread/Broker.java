package com.epam.training.multithread;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.TimeUnit;

public class Broker {
  private Integer capacity;
  private ArrayBlockingQueue<String> queue;
  private Boolean continueProducing = Boolean.TRUE;

  public Broker(Integer capacity) {
    this.capacity = capacity;
    queue = new ArrayBlockingQueue<>(capacity);
  }

  public void put(String data) throws InterruptedException {
    this.queue.put(data);
  }

  public String get() throws InterruptedException {
    return this.queue.poll(5, TimeUnit.MILLISECONDS);
  }

  public Boolean isFull() {
    return queue.size() >= capacity;
  }

  public void setContinueProducing(Boolean continueProducing) {
    this.continueProducing = continueProducing;
  }

  public Boolean getContinueProducing() {
    return continueProducing;
  }
}
