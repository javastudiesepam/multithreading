package com.epam.training.multithread;

public class Consumer implements Runnable {
  private String name;
  private Broker broker;
  private String password;

  public Consumer(String name, String password, Broker broker) {
    this.name = name;
    this.broker = broker;
    this.password = password;
  }

  @Override
  public void run() {
    long startTime = System.currentTimeMillis();

    try {
      String data = broker.get();
      while (broker.getContinueProducing()) {
        if  (data != null) {
          String attemptHashCode = HashCalculator.hash(data);
          if (attemptHashCode.equals(password)) {
            System.out.println("Password Found: " + data);
            broker.setContinueProducing(Boolean.FALSE);
            break;
          }

          data = broker.get();
        }
      }
    } catch (InterruptedException e) {
      e.printStackTrace();
    }
    long stopTime = System.currentTimeMillis();
    System.out.println(stopTime - startTime);

    System.out.println("Consumer " + this.name + " finished its job; terminating.");
  }
}
